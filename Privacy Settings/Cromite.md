# Cromite Privacy Settings

Go to Settings.



## Search engine
- DuckDuckGo



## Password Manager
- Save passwords: Off
- Auto Sign-in: Off

NOTE: Never save passwords in browsers. Use a password manager instead. Check out [recommendations, alternatives & reviews](https://github.com/StellarSand/privacy-settings#recommendations-alternatives--reviews).



## Payment methods
- Save and fill payment methods: Off

NOTE: Never save payment info in browsers. A password manager can be used to also save payment information.



## Addresses and more
- Save and fill addresses: Off



## Privacy and security
- Safe Browsing >
  - Standard Protection
  - Standard Protection > Help improve security on the web: Off
- Always use secure connections: On
- Access payment methods: Off
- Preload pages: No preloading
- Use secure DNS >
  - On
  - Choose another provider > NextDNS/Cloudflare or select Custom and provide URL for other secure DNS from [this list](https://www.privacyguides.org/dns/)

    Example: For Quad9, enter https://dns.quad9.net/dns-query

- Always incognito mode > Enable history: Off
- Send a "Do Not Track" request: On
- Privacy Sandbox >
  - Trials: Off
- Improve search suggestions: Off
- Touch to Search: Off



## Adblock Plus settings
- Enable Adblock Plus: On
- Filter lists: Select lists based on your language
- Enable anti-circumvention and snippets: On



## Legacy Adblock settings
- On



## Site settings
- Cookies: Block third-party cookies
- Ads: Off
- Your device use: Off
- JavaScript JIT: Off
- Timezone override: Random
- Viewport Size Protection: On



## Downloads
- Ask where to save files: On



## Google services
- Allow Cromite sign-in: Off
- Help improve Cromite's features and performance: Off
- Make searches and browsing better: Off
- Improve search suggestions: Off
- Touch to Search: Off