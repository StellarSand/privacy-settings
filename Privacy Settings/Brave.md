# Brave Privacy Settings

Go to Settings



## Appearance
- Show Brave News button in address bar: Off
- Always show full URLs: On
- Show autocomplete suggestions in address bar >
  - Top sites: Off
  - Leo AI Assistant: Off



## Shields
- Auto-redirect AMP pages: On
- Auto-redirect tracking URLs: On
- Prevent sites from fingerprinting me based on my language preferences: On
- Trackers and ads blocking: Aggressive
- Upgrade connections to HTTPS: Strict
- Block fingerprinting: Strict (If you face any issues with sites, change it back to standard)
- Block cookies: Only cross-site



## Brave Rewards
- Show Brave Rewards icon in address bar: Off
- Show Tip button on sites: Off
- Open full Brave Rewards settings > 
  - New tab page ads: Off
  - Notification ads: Off



## Privacy and security
- Use Google services for push messaging: Off
- Allow privacy-preserving product analytics (P3A): Off
- Automatically send daily usage ping to Brave: Off
- Automatically send diagnostic reports: Off

#### Cookies and other site data
- Block third-party cookies
- Send a "Do Not Track" Request with your browser traffic: On

#### Security
- Standard protection
- Use secure DNS >
  - On
  - With > Cloudflare/NextDNS or select Custom and provide URL for other secure DNS from [this list](https://www.privacyguides.org/dns/)

    Example: For Quad9, enter https://dns.quad9.net/dns-query

#### Site and Shields Settings
- Additional permissions >
  - Ethereum: Block sites from accessing the Ethereum provider API
  - Solana: Block sites from accessing the Solana provider API



## Web3

#### Wallet
- Show Brave Wallet icon on toolbar: Off
- Enable NFT discovery: Off
- Automatically pin NFTs: Off

#### Web3 Domains
- Resolve Unstoppable Domains domain names: Disabled
- Resolve Ethereum Name Service (ENS) domain names: Disabled
- Resolve Solana Name Service (SNS) domain names: Disabled



## Leo
- Show Leo icon in the sidebar: Off
- Show suggested prompts in the conversation: Off



## Search engine
- Normal window: DuckDuckGo/Brave/Startpage or any other 
privacy respecting search engine from `Manage search engines and site search`.
- Private window: DuckDuckGo/Brave/Startpage or any other 
privacy respecting search engine from `Manage search engines and site search`.
- Improve search suggestions: Off
- Web Discovery Project: Off



## Autofill and passwords

#### Password Manager
- Settings >
  - Offer to save passwords: Off
  - Auto Sign-in: Off

NOTE: Never save passwords in browsers. Use a password manager instead. Check out [recommendations, alternatives & reviews](https://github.com/StellarSand/privacy-settings#recommendations-alternatives--reviews).

#### Payment methods
- Save and fill payment methods: Off
- Allow sites to check if you have payment methods saved: Off

NOTE: Never save payment info in browsers. A password manager can be used to also save payment information.

#### Addresses and more
- Save and fill addresses: Off

#### Allow auto-fill in private windows
- Off



## Languages
- Spell check: Off



## Downloads
- Ask where to save each file before downloading: On


---
---


Open a new tab. Click on Customize at the bottom

## Background Image
- Show Sponsored Images: Off



## Brave News
- Show Brave News: Off



## Cards
- Click Hide for all of them
- Cards: Off
