# Microsoft Edge Mobile Privacy Settings

Go to Settings



## Accounts

#### Passwords
- Save passwords: Off
- Auto Sign-in: Off
- Autofill for other apps: Off
 
 NOTE: Never save passwords in browsers. Use a password manager instead. Check out [recommendations, alternatives & reviews](https://github.com/StellarSand/privacy-settings#recommendations-alternatives--reviews).
 
 #### Addresses and more
- Save and fill addresses: Off

#### Payment info
- Save and fill payment info: Off

NOTE: Never save payment info in browsers. A password manager can be used to also save payment information.



## Privacy and security

#### Diagnostic data
- Optional diagnostic data: Off

#### Site settings
- Cookies: Block third-party cookies
- Ads: Off
- Your device use: Off

#### Show me search and site suggestions using my typed characters
- Off

#### Tracking prevention
- On
- Strict

#### Block ads
- On
- Allow acceptable ads: Off

#### Do Not Track
- On



## General
- Select search engine: DuckDuckGo
- Shopping: Off



---
---

On home page > Tap icon on top right > Custom >
 - Show feeds: Off
