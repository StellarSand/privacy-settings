# Microsoft Edge Privacy Settings

Go to Settings



## Profiles

#### Microsoft Rewards
- Earn Microsoft Rewards in Microsoft Edge: Off

#### Personal info
- Save and fill basic info: Off
- Save and fill custom info: Off

#### Passwords
- Offer to save passwords: Off
- Autofill passwords: Off

NOTE: Never save passwords in browsers. Use a password manager instead. Check out [recommendations, alternatives & reviews](https://github.com/StellarSand/privacy-settings#recommendations-alternatives--reviews).

#### Payment info
- Save and fill payment info: Off
- Show Express checkout on sites when you shop: Off
- Cards you've saved on this device: Remove any payment info if added

NOTE: Never save payment info in browsers. A password manager can be used to also save payment information.

#### Share browsing data with other Windows features
- Share browsing data with other Windows features: Off



## Privacy, search, and services

#### Tracking prevention
- On
- Strict

#### Privacy
- Send "Do Not Track" requests: On
- Allow sites to check if you have payment methods saved: Off

#### Optional diagnostic data
- Help improve Microsoft products by sending optional diagnostic ata about how you use the browser, websites you visit, and crash reports: Off

#### Search and service improvement
- Help improve Microsoft products by sending the results from searches on the web: Off

#### Personalization and advertising
- Allow Microsoft to use your browsing activity including history, favorites, usage and other browsing data to personalize Microsoft Edge and Microsoft services like ads, search, shopping and news: Off


#### Security
- Website Typo Protection: On
- Use secure DNS to specify how to lookup the network address for websites >
  - On
  - Choose a service provider > NextDNS/Cloudflare or provide URL for other secure DNS from [this list](https://www.privacyguides.org/dns/)

  Example: For Quad9, enter https://dns.quad9.net/dns-query

- Enhance your security on the web >
  - On
  - Strict


#### Services
- Save time and money with Shopping in Microsoft Edge: Off (use something else like [Keepa](https://keepa.com/), [camelcamelcamel](https://camelcamelcamel.com/), [Price History](https://pricehistoryapp.com/) etc)
- Get notifications of related things you can explore with Discover: Off
- Include related matches in Find on Page: Off

#### Address bar and search
- Show me search and site suggestions using my typed characters: Off
- Search engine used in address bar: DuckDuckGo or add any other privacy respecting search engine from Manage search engines.
- Search on new tabs uses search box or address bar: Address bar (if `Search box` is selected it will continue searching with Bing as the search engine)



## Sidebar
- Personalize my top sites in customize sidebar: Off

#### App and notification settings
- Copilot >
  - Show Copilot: Off
  - Show shopping notifications: Off
- Action center >
  - Automatically open Action Center to see how you can be rewarded for using Microsoft Edge: Off
- Search > Automatically open Search: Off
- Games >
  - Automatically open Casual Games in the side pane: Off
  - Allow access to page URLs: Off



## Start, home, and new tabs

#### New tab page
- Preload the new tab page for a faster experience: Off



## Cookies and site permissions

#### Manage and delete cookies and site data
- Block third party cookies: On
- Preload pages for faster browsing and searching: Off



## Downloads
- Ask me what to do with each download: On
- Show downloads menu when a download starts: On



## Languages
- Use text prediction: Off
- Enable grammar and spellcheck assistance: Off

#### Share additional OS regional format
- Share additional operating system region: Never


---
---


Open a new tab. Click the gear icon on top right

## Page settings
- Layout: Custom
- Show sponsored links: Off
- Show greeting: Off
- Content: Content off